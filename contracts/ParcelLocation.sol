pragma solidity ^0.4.18;

contract ParcelLocation {
    address owner;

    string public location;

    /* Constructor */
    function ParcelLocation (string initialLocation) public {
        owner = msg.sender;
        location = initialLocation;
    }

    function updateLocation (string newLocation) public {
        if (msg.sender == owner) {
            location = newLocation;
        }
    }


    function () public payable {

    }

    function kill () private {
        if (msg.sender == owner) {
            selfdestruct(owner);
        }
    }
}