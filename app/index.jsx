import React from 'react';
import ReactDOM from 'react-dom';

import './index.scss';

import Header from './components/header/header.component.jsx';

import Web3Init from './modules/web3Init';
import EthAddressChecker from './components/ethAddressChecker/ethAddressChecker.component.jsx';
import EthBalanceChecker from './components/ethBalanceChecker/ethBalanceChecker.component';

Web3Init();

class App extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <React.Fragment>
                <Header />
                <main className="mdl-layout__content">
                    <div className="page-content">
                        <EthAddressChecker />
                        <EthBalanceChecker />
                    </div>
                </main>
            </React.Fragment>
        );
    }
}

ReactDOM.render(<App/>, document.getElementById('app'));