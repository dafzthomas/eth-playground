const Web3Service = {
    isAddress: address => {
        if (window.Web3.isAddress === undefined) {
            return window.Web3.utils.isAddress(address);
        } else {
            return window.Web3.isAddress(address);
        }

    },
    fromWei: balance => {
        if (window.Web3.fromWei === undefined) {
            return window.Web3.utils.fromWei(balance);
        } else {
            return window.Web3.fromWei(balance);
        }
    },
    getBalance: (...opts) => {
        return window.Web3.eth.getBalance(...opts);
    }
}

export default Web3Service;
