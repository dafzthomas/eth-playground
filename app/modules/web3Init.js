
function Web3Init () {
    if (typeof web3 !== 'undefined') {
        console.log('Web3 Detected! ' + web3.currentProvider.constructor.name)
        window.Web3 = new Web3(web3.currentProvider);
    } else {
        console.log('No Web3 Detected... ');

        import('web3').then(obj => {
            window.Web3 = new obj.default(new obj.default.providers.HttpProvider("https://mainnet.infura.io/mQJfxXv9Fdp6JOF5ZntF"));
        })
    }
}

module.exports = Web3Init;