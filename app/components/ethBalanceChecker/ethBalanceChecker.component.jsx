import React from 'react';

import Web3Service from './../../modules/web3.service';

class EthBalanceChecker extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            balance: 0,
            inputValue: ""
        };

        this.getBalance = this.getBalance.bind(this);
        this.handleInputValue = this.handleInputValue.bind(this);
        this.setBalanceOnScreen = this.setBalanceOnScreen.bind(this);
    }

    handleInputValue (event) {
        this.setState({
            inputValue: event.target.value
        })
    }

    setBalanceOnScreen (balance) {
        const displayEth = Web3Service.fromWei(balance.toString());

        this.setState({
            balance: displayEth
        });
    }

    getBalance () {
        const address = this.state.inputValue;

        const addressBalance = Web3Service.getBalance(address, function (error, balance) {
            this.setBalanceOnScreen(balance);
        }.bind(this));
    }

    render() {
        return (
            <div className="demo-card-wide mdl-card mdl-shadow--2dp">
                <div className="mdl-card__title">
                    <h2 className="mdl-card__title-text">Balance Checker</h2>
                </div>

                <div className="mdl-card__supporting-text">
                    <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input className="mdl-textfield__input"
                               type="text"
                               id="is_address_input"
                               value={this.state.inputValue}
                               onChange={this.handleInputValue} />
                        <label className="mdl-textfield__label" htmlFor="is_address_input">Address</label>
                    </div>

                    <p><strong>Balance (ETH): </strong> {this.state.balance}</p>

                    <div className="mdl-card__actions">
                        <button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
                                onClick={this.getBalance}>
                            Check
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default EthBalanceChecker;