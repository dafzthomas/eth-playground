import React from 'react';

import Web3Service from './../../modules/web3.service';

class EthAddressChecker extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            validAddress: null,
            inputValue: ""
        };

        this.isEthAddress = this.isEthAddress.bind(this);
        this.handleInputValue = this.handleInputValue.bind(this);
    }

    handleInputValue (event) {
        this.setState({
            inputValue: event.target.value
        })
    }

    isEthAddress () {
        const address = this.state.inputValue;

        const isAddress = Web3Service.isAddress(address);

        this.setState({
            validAddress: isAddress
        });
    }

    render() {
        return (
            <div className="demo-card-wide mdl-card mdl-shadow--2dp">
                <div className="mdl-card__title">
                    <h2 className="mdl-card__title-text">Address Checker</h2>
                </div>

                <div className="mdl-card__supporting-text">
                    <p>Enter an ETH address to check if it's valid.</p>

                    <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input className="mdl-textfield__input"
                               type="text"
                               id="is_address_input"
                               value={this.state.inputValue}
                               onChange={this.handleInputValue} />
                        <label className="mdl-textfield__label" htmlFor="is_address_input">Address</label>
                    </div>

                    {
                        this.state.validAddress ? (
                            <span className="mdl-chip">
                                <span className="mdl-chip__text green" id="is_address_result">Valid address</span>
                            </span>
                        ) : (
                            <span className="mdl-chip">
                                <span className="mdl-chip__text red" id="is_address_result">{ this.state.validAddress === null ? ("Please enter an address") : ("Invalid address") }</span>
                            </span>
                        )
                    }

                    <div className="mdl-card__actions">
                        <button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
                                onClick={this.isEthAddress}>
                            Check
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default EthAddressChecker;