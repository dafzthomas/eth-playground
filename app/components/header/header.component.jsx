import React from 'react';

import './_header.scss';

class Header extends React.Component {
    constructor (props) {
        super(props);

    }

    render () {
        return (
            <header>
                <h1>Ethereum Playground</h1>
            </header>
        );
    }
}

export default Header;